package com.sr.cosmos;

import com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends ReactiveCosmosRepository<Items, String>{

}
