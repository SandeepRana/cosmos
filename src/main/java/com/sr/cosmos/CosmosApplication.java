package com.sr.cosmos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
@ComponentScans({ @ComponentScan("com.sr.cosmos") })
public class CosmosApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(CosmosApplication.class);


    
	public static void main(String[] args) {
		SpringApplication.run(CosmosApplication.class, args);
	}
	
	/*
	 * public void run(String... var1) { // this.repository.deleteAll().block();
	 * LOGGER.info("Deleted all data in container.");
	 * 
	 * final Items testUser = new Items("11", "a", "b", "c", true);
	 * 
	 * // Save the User class to Azure Cosmos DB database. final Mono<Items>
	 * saveUserMono = repository.save(testUser);
	 * 
	 * LOGGER.info("Added all data in container." + saveUserMono.block());
	 * 
	 * final Mono<Items> getUserMono = repository.findById("1");
	 * 
	 * LOGGER.info("DATA--" + getUserMono.block()); }
	 */

}
