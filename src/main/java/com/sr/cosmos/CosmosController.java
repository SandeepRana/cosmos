package com.sr.cosmos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class CosmosController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CosmosController.class);

	@Autowired
	private ItemRepository repository;

	@GetMapping(path = "/items/{i}")
	public Items getItems(@PathVariable String i) {

		final Mono<Items> getUserMono = repository.findById(i);

		LOGGER.info("DATA--" + getUserMono.block());

		return getUserMono.block();

	}

	@PostMapping("/SaveItems")
	public String saveItems(@RequestBody Items item) {

		final Items testUser = new Items(item.getId(), item.getCategory(), item.getName(), item.getDescription(), item.isComplete());

		// Save the User class to Azure Cosmos DB database. 
		final Mono<Items> saveUserMono = repository.save(testUser);

		LOGGER.info("Added all data in container." + saveUserMono.block());

		return "Data saved successfully";

	}
}
